package br.com.challenge;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ToJson<T extends Object> {
	
	private static final String COLON = ":";
	private static final String IKEYS = "{";
	private static final String EKEYS = "}";
	private static final String IBRACKET = "[";
	private static final String EBRACKET = "]";
	private static final String COMMA = ",";
	private static final String QUOTES = "\"";
	private final static Logger LOGGER = Logger.getLogger("ToJson"); 
	
	/**
	 * 
	 * @param t Object to parse to Json
	 * @return the Json String
	 * @throws IllegalAccessException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 */
	public String toJson(T t) throws IllegalAccessException, ClassNotFoundException{
		LOGGER.setLevel(Level.INFO);
		LOGGER.info("Create builder");
		StringBuilder builder = new StringBuilder();
		builder.append(IKEYS);
		LOGGER.info("Get name from class ");
		Class<?> c = Class.forName(t.getClass().getName());
		builder.append(QUOTES+c.getSimpleName().toLowerCase()+QUOTES);
		builder.append(COLON);
		builder.append(IBRACKET);
		builder.append(IKEYS);
		int sumFields = 0;
		LOGGER.info("Get object's fields ");
		for (Field f : t.getClass().getDeclaredFields()) {
			getValueAndNameFromField(t, builder, f);
			while(++sumFields < t.getClass().getDeclaredFields().length){
				builder.append(COMMA);
				break;
			}
		}
		builder.append(EKEYS);
		builder.append(EBRACKET);
		builder.append(EKEYS);
		return builder.toString();
	}

	private void getValueAndNameFromField(T t, StringBuilder builder, Field f) throws IllegalAccessException {
		LOGGER.info("Get field's value");
		f.setAccessible(true);
		builder.append(QUOTES+f.getName().toLowerCase()+QUOTES);
		builder.append(COLON);
		if(f.getType().isArray()){
			LOGGER.info("Field is array.");
			getFieldsFromArray(f, t, builder);
		} else {
			LOGGER.info("Field is not array...");
			Object value = f.get(t);
			LOGGER.info("Value is "+value);
			appendValue(value, builder);
		}
	}

	private void appendValue(Object value, StringBuilder builder) {
		LOGGER.info("Put QUOTES in field which is String ou character");
		if(value instanceof String || value instanceof Character){
			builder.append(QUOTES);
			builder.append(value);
			builder.append(QUOTES);
		} else {
			builder.append(value);
		}
	}
	
	private void getFieldsFromArray(Field f, T t, StringBuilder builder) throws IllegalArgumentException, IllegalAccessException{
		LOGGER.info("Get value from field in array");
		Object array = f.get(t);
		int length = Array.getLength(array);
		int comma = 0;
		builder.append(IBRACKET);
		for (int i = 0; i < length; i++){
		    Object value = Array.get(array, i);
		    LOGGER.info("Value is "+value);
		    appendValue(value, builder);
		    while(++comma<length){
				builder.append(COMMA);
				break;
			}
		}
		builder.append(EBRACKET);
		LOGGER.info("Finished to get values");
	}
	
}