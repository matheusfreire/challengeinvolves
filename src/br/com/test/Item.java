package br.com.test;

import java.util.ArrayList;
import java.util.List;

public class Item {
	
	private int code;
	private String description;
	private List<String> names;
	private int a[] = {1,2};

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getNames() {
		if (names == null) {
			names = new ArrayList<String>();
		}
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public int[] getA() {
		return a;
	}

	public void setA(int[] a) {
		this.a = a;
	}
	
}