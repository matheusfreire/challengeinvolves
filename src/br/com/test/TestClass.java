package br.com.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import br.com.challenge.ToJson;


public class TestClass {
	
	private ToJson<Item> toJson;
	private Item item;
	
	@Before
	public void composeObjectToParse(){
		toJson = new ToJson<Item>();
		item = new Item();
		item.setCode(2);
		item.setDescription("a test");
		List<String> listOfNames = new ArrayList<String>();
		listOfNames.add("abc");
		listOfNames.add("def");
		item.setNames(listOfNames);
	}
	
	@org.junit.Test
	public void assertPrintJson(){
		try {
			assertNotNull(toJson);
			assertNotNull(item);
			assertEquals("{\"item\":[{\"code\":2,\"description\":\"a test\",\"names\":[abc, def],\"a\":[1,2]}]}",
					toJson.toJson(item));
		} catch (IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
